@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Show</h1>
        <div class="w-50 m-auto">
            <div style="width: 150px; height: 150px">
                <img src="{{ $product->thumbnail }}" alt="asd" style="width: 100%; height: 100%; object-fit: cover">
            </div>
            <p>
                Name = {{$product->name}}
            </p>
            <p>
                Description = {{$product->description}}
            </p>
            <p>
                Price = {{$product->price}}
            </p>
            <p>
                Owner = {{Auth::user()->name}}
            </p>
        </div>
    </div>
@endsection
