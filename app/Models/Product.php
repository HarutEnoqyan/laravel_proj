<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    protected $fillable = ['user_id', 'name', 'description', 'price'];

    protected $appends = ['thumbnail'];

    const IMAGE_PATH = 'public/products/thumbnail/';

    protected $default_image_path = 'public/img/products/default_thumbnail.png';
    protected $public_path = '/storage/products/thumbnail/';

    public function isProductOf(User $user)  {
        return $this->user_id = $user->id;
    }

    public function getThumbnailAttribute() {
        $path = self::IMAGE_PATH . $this->id . '.jpeg';
        if(Storage::disk('local')->exists($path )){
            return $this->public_path .  $this->id . '.jpeg';
        }else {
            return $this->default_image_path;
        }
    }
}
